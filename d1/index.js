//console.log("Fighting");


//array methods
//JS has built-in functions and methods for arrays. this allows us to manipulate and access our array items

//Mutator Methods

/*
		-Mutator Methods are functions that 'mutate' or change an array after they are created
		-THese methods manipulate the original array performing various tasks such as adding and removing elements
*/


let fruits = ['Apple','Orange','Kiwi','Dragon Fruit'];

//push()
/*
		-adds an element in the end of an array AND returns the array's length

		-SYntax
			arrayName.push();
*/

console.log('Current Array: ');
console.log(fruits); // log fruits array
let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);//5
console.log('Mutated array from push method: ');
console.log(fruits);//Mango is added at the end of the array

fruits.push('Avocado', 'Guava');
console.log("Mutated array from push method: ")
console.log(fruits);// adds the 2 in the end


//pop();
/*
	Removes the last element in an array AND returns the removed element
	-Syntax
		arrayName.pop();
*/

let removedFruit = fruits.pop();
console.log(removedFruit);// removes the last item in the array and return it // Guava
console.log('Mutated array from pop method: ');
console.log(fruits);//Guava is removed



let ghostFighters = ["Eugene","Dennis",'Alfred',"Taguro"];
function unfriend(){
		ghostFighters.pop();
};
unfriend(); // invokes unfriend function
console.log(ghostFighters);

//unshift()
/*
		-adds one or more element(s) at the beginning of an array
		-Syntax
			arrayName.unshift('elementA');
			arrayName.unshift('elementA', 'elementB');

*/

fruits.unshift("Lime","Banana");
console.log("Mutated array from unshift method: ");
console.log(fruits);

//shift()

/*
		removes an element at the beginning of an array AND returns the removed element
		-Syntax
			arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);//Lime
console.log("Mutated array from shift method:");
console.log(fruits);//the first array Lime element is removed from the array


//splice
/*
		simultaneously removes elements from a specified index number and adds elements
		-Syntax
			arrayName.splice(startingIndex,deleteCount,elementsToBeAdded);
*/

fruits.splice(1, 2, "Lime", "Cherry",);//replaced the element starting in index '1' then delete 2elements , then elements to be added
console.log('Mutated array from splice method');
console.log(fruits);

//sort
/*
	Rearranges the array elements in alphanumeric order
	-Syntax
		arrayName.sort();
*/
	fruits.sort();
	console.log("Mutated array from sort method:")
	console.log(fruits); // arranged alphabetically
//reverse
/*
	Reverses the order of array elements
	-Syntax
		arrayName.reverse();
*/
	fruits.reverse();
	console.log("Mutated array from reverse method:")
	console.log(fruits);//arranged reversed alpha

//Non-Mutator methods are functions that do not modify or change an array after they are created
/*
	Non-Mutator methods are functions that do not modify or change an array after they are created
	-these methods do not manipulate the original array performing various tasks such as returning elements from an array and cimbining arrays and printing the output
*/

let countries = ['Us', 'PH' , 'CAN', 'SG' , 'TH' , 'PH', 'FR', 'DE'];

////indexOf()

/*
		returns the index number of the first matching element found in an array
		-if no match is found, the result is -1
		-the search process will be done from first element processing to the last element
		-Syntax
			arrayName/indexOf(searchValue);
			arrayName/indexOf(searchValue,fromIndex);
*/

let firstIndex = countries.indexOf('PH');
console.log("Result of indexOf method: " + firstIndex); // gives index no of PH equals !

let invdalidCountry = countries.indexOf('BR');
console.log("Result of indexOf method: " + invdalidCountry);// will return negative 1 because it does not exist

//lastIndexOf()
/*
	Returns the index number of the last matching element found in an array 
	-the search process will be done from the last element processing to the first element
	Syntax
		arrayName.lastIndexOf(searchvalue);
		arrayName.lastIndexOf(searchvalue, fromIndex);
*/

let lastIndex = countries.lastIndexOf('PH');
console.log("Result of lastIndexOf method: " + lastIndex);

//slice()

/*
		-portions/slices elements from an array AND returns a new array
		-Syntax
			arrayName.slice(startingIndex);
			arrayName.slice(startingIndex,endingIndex);
*/

//['Us', 'PH' , 'CAN', 'SG' , 'TH' , 'PH', 'FR', 'DE'];
let slicedArrayA = countries.slice(2);//starting index given "2"
console.log("Result from slice method: ");
console.log(slicedArrayA);//starts from 2 then returns the following

let slicedArrayB = countries.slice(2,4);//starting and ending index given 
console.log(slicedArrayB); // 'CAN' and "SG" di kasama yung ending si 'TH'

let slicedArrayC = countries.slice(-3);
console.log("Result from sllce method: ");
console.log(slicedArrayC);//count from zero pabalik 0 -1 -2 -3 returns // 'PH' "FR" " DE"

//toString();
/*
		Returns an array as a string separated by commads
		-Syntax
		arrayName.toString();
*/

let stringArray = countries.toString();
console.log("Result from toString methds: ");
console.log(stringArray);

//concat()
/*
	combines two arrays and returns the combined result
	-Syntax
		arrayA.concat(arrayB);
		arrayA.concat(elementA);
*/

let taskArrayA = ['drink html', 'eat javascript'];
let taskArrayB = ['inhale css', 'breathe sass'];
let taskArrayC = ['get git', 'be node'];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Result from concat method: ");
console.log(tasks);

//Combine multiple arrays
console.log("Result from concat method: ");
let allTasks = taskArrayA.concat(taskArrayB,taskArrayC);
console.log(allTasks);

//Combining arrays with elements
let combinedTasks = taskArrayA.concat('smell express', 'throw react');
console.log("Result from concat method:");
console.log(combinedTasks);

//join()

/*
	-Returns an array as a string separated by specified separator
	-Syntax
		arrayName.join('separatorString');
*/
let users = ["John",'Jane','Joe','Robert','Nej'];
console.log(users.join());
console.log(users.join(''));//separates with nothing
console.log(users.join(' - '));//separates with -

//Iteration Method

/*
		Iteration methods are loops designed to perform repetitive tasks on arrays
		Iteration methods loops over all items in an array
		Useful for manipulating array data resulting in complex tasks
*/

//forEach
/*
		Similar to a for loop that iterates on each array element
		-for each item in the array, the anonymous function passed in the forEach() method will be run
		-the anonymous function is able to receive the current item being iterated or loop over by assigning a parameter
		-variable names for arrays are normally written in the plural form data stored in an array
		-it's common practice to use the singular form of the array content for parameter names used in array loops 
		-forEach() does NOT return anything
		-Syntax
			arrayName.forEach(function(individualElement){
					statement
			})
*/

//let ghostFighters = ["Eugene","Dennis",'Alfred',"Taguro"];
	allTasks.forEach(function(task){
		console.log(task);
	});

	function getFighters(){
	ghostFighters.forEach(function(fighters){
		console.log(fighters);
	});
};
getFighters();


//Using forEach with conditional statements

//Looping through all array items
/*
		it is a good practice to print the current element in the console when working with array iteration methods to have an idea of what information is being worked on for each iteration of the loop

		creating a separate variable to store results of an array iteration 
		iteration methods are also good practice to avoid consuion by modifying the original array

		Mastering loops and arrays allows us developers to perform  a wide range of features that help in data management and ananlysis
*/

//let taskArrayA = ['drink html', 'eat javascript'];
//let taskArrayB = ['inhale css', 'breathe sass'];
//let taskArrayC = ['get git', 'be node'];

let filteredTasks = [];

allTasks.forEach(function(task){

	console.log(task);

	if(task.length>10){
		console.log(task);
		filteredTasks.push(task)
	}

});

console.log("Result of filteredTasks: ");
console.log(filteredTasks);

//map()

	/*
		-iterates on each element AND returns new array with different values depending on the result of the function's operation

		-Syntax
			let/const resultArray = arrayName.map(function(individualElement))
	*/


	let numbers = [1, 2, 3, 4, 5];//1-5 are th individual elemetn called number

	let numberMap = numbers.map(function(number){
		return number * number;
	});

	console.log("original Array: ");
	console.log(numbers);
	console.log("Result	of map method: ");
	console.log(numberMap);

	//map() vs forEach()

		let numberForEach = numbers.forEach(function(number){
		return number * number;
	});
		console.log(numberForEach);//undefined beacuse it does not return anything

//every()
/*
	-checks if all elements in an array meet the given conditions
	-this is useful for validating data stored in arrays especially when dealing with large amounts of data
	-returns true if all elements meet the condition, otherwise false
	-Syntax
	let/const resultArray = arrayName.every(function(indivElement){
			return expression/condition;
	})


*/

let allValid = numbers.every(function(number){
	return (number <3);
});
console.log("result of every method:");
console.log(allValid);//false

//some()
/*
	Check if atleast one element in the array meets the given condition 
	-returns a true value if at least one element meets the condition and false if otherwise
	Syntax
		let/const resultArray = arrayName.some(function(indivElement){
		return expression/condition;
		})
*/

let someValid = numbers.some(function(number){
	return (number<2);
})

console.log("Result of some method: ");
console.log(someValid);//true


//Combining the returned result from every/some method may be used in other statements to perform consecutive results
if(someValid){
	console.log("Some numbers in the array are greater than 2");
}

//filter
/*
		-returns a new array that contains elements which meets a given condition 
		-returns an empty array if no elements were found
		Syntax:

		let/const resultArray = arrayName.filter(function(indivElement){
		return expression/condition;
		})
*/

let filterValid = numbers.filter(function(number){
	return (number<3);
}); // expected out put 1 & 2;

console.log("Result of filter method: ");
console.log(filterValid);//[1,2]

let nothingFound = numbers.filter(function(number){
	return (number = 0);
});

console.log("Result of filter method: ");
console.log(nothingFound);//empty array

//filtering for each

let filteredNumbers = []

numbers.forEach(function(number){
	//console.log(number);

	if(number<3){
		filteredNumbers.push(number);
	}
});
console.log("Result of filter method: ");
console.log(filteredNumbers);


//includes()
/*
	checks if the arguments passed can be found in the array
		-it returns a boolean which can be savednin a variable
			-returns true if the arguments is found in th array
			-returns false it it is not

			-Syntax:
				arrayName.includes(<argumentToFind>)

*/

let products = ['Mouse', "Keyboard", "Laptop", 'Monitor'];
console.log(products)

let productFound = products.includes("Mouse");
console.log("is the mouse in the products "+ productFound);
console.log(productFound);//true

let productNotFound = products.includes("Headset");
console.log("is headset in the products "+ productNotFound);
console.log(productNotFound);//false

//Method Chaining
	//Methods can be chained using them one after another
	//THe result of the first method is used on the second method until all "chained" methods have been resolved


let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes('a');
});

console.log(filteredProducts);


let contacts = ['Ash'];

function addTrainer(trainer){
	let filteredTrainer = contacts.includes(trainer);

		if (filteredTrainer){
			console.log("Already added in the Match Call")
		}else {
			contacts.push(trainer);
			console.log("Registered");
		}
}
console.log(contacts);
addTrainer("Brock")
console.log(contacts)
addTrainer("Ash");


//reduce 
/*

	-evaluates elements from left to right and returns/reduces the array into a single value
	-Syntax
	let/const resultArray = arrayName.reduce(function(accumulator,currentValue){
		return expression/operation
	})

	-"accumulator" parameter in the function stores the result for every iteration of the loop
	-'currentValue' is the current/next element in the array that is evaluated in each iteration of the loop



How the "reduce" method works
1. the first/result element in the array is stored in the "accumulator" parameter
2. the seconde/next element in the array is stored in the currentValue parameter
3. an operation is performed on the two elements

*/

console.log(numbers);
let iteration = 0;
let iterationStr = 0;

let reducedArray = numbers.reduce(function(x,y){
	console.warn('Current iteration: '+ ++iteration);
	console.log("Accumulator: " + x);
	console.log("current value: " + y);


	return x + y;
});

console.log("Result of  Reduced method : " + reducedArray);//15

let list = ['Hello', 'Again', 'World'];

let reducedJoin = list.reduce(function(x,y){
	console.warn('Current iteration: '+ ++iterationStr);
	console.log("Accumulator: " + x);
	console.log("current value: " + y);


	return x + ' ' + y;
})
console.log("Result of reduce method " + reducedJoin);